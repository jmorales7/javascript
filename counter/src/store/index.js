import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    getCounter: state => state.counter
  },
  mutations: {
    setCounter: (state) => {
      state.counter ++
    },
    setDiscountCounter: (state) => {
      state.counter --
    },

  },
  actions: {
    addCounter({commit}) {
      commit('setCounter')
    },
    discountCounter({commit}) {
      commit('setDiscountCounter')
    }
  },
  modules: {
  }
})

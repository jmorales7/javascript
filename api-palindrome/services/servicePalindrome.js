module.exports = {
    checkPalindrome: (phrase) => {
        let j = phrase.length-1;
        let inverse = "";

        for ( let i = 0; i<= phrase.length-1; i++)
        {
            inverse = inverse + phrase[j]
            j--
        }
        if(phrase === inverse) {
            return true;
        }else {
            return false;
        }

    }
};
const express = require('express');

const app = express();

app.use(require('./palindrome'));

module.exports = app;
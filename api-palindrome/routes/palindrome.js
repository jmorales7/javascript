const express = require('express');
const {checkPalindrome} = require('../services/servicePalindrome');

const app = express();

app.post('/palindrome',(req,res) => {
    let phrase = req.body.phrase;
    let isPalindrome = false;
    if(!phrase) {
        return res.status(500).json({
            status: false,
            message: 'phrase is required'
        });
    }

    isPalindrome = checkPalindrome(phrase);
    if (isPalindrome) {
        return res.status(200).json({
            phrase: phrase,
            palindrome: true
        });
    } else {
        return res.status(200).json({
            phrase: phrase,
            palindrome: false
        });
    }

});

module.exports = app;
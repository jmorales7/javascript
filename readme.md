## Test

This directory contains the three exercises that were done:
- Counter-function.
- Api palindrome.
- Counter
.


## Counter Function

In the exercise the  term was handled was Closure. A closure is a function that has access to its external function scope. This means that a closure can remember and access variables and arguments of its external function even after the function has finished.
That is why in the exercise a nested function was handled which in each invocation remembers the previous value of the counter variable. This is related to the concepts of Execution Context and Lexical Environment in javascript.


## Api-Palindrome

The palindrome exercise was done with a structure to emulate a workflow of a real project and try to do it in a modularized way. a route directory a configuration directory and a services directory were defined. In the directory of services is where the verification is made to know if it is palindrome or not.
## Counter

The counter was made with vue js where different tools were used to simulate a more prepared work environment



